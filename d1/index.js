//What are conditional statements?
    //Conditional Statements allow us to control the flow of our page 
    //It allows us to run a statement/intructures if a condition is met or run another separate instructin is otherwise


    let numA = -1;


//if Statement
    //execute a statement if a specified condition is true
    if(numA < 0){
        console.log("numA is less than 0");
    }

//the resukt of the expression added in the if's condition must result to true, else, the statement inside the if() will not run

console.log(numA < 0);//true

numA = 0;

if(numA < 0){
    console.log("if numA is 0");
}

let city = "New York";

if(city === "New York"){
    console.log("Welcome to New York city!");
}

//else if Clause
//Executes a statement if previous condition are flase and if the condition id true
//the "else if clause is optional and can be added to capture additional condition"
let numH = 1;

if(numA < 0){
    console.log("If statement will run");
}else if(numH > 0){
    console.log("Else statement will run");
};

numA = 1;

if(numA > 0){
    console.log("the number is greater than zero, the if statement runs") 
} else if (numH > 0){
    console.log("Iam the else if statement")
}

city = "Tokyo";

if(city === "New York"){
    console.log("Welcome to New York city!");
}else if(city === "Tokyo"){
    console.log("Welcome to Tokyo, Japan!");
}

numA = -1;
numH = 1;

//Else statement 
        //will execute if all other condition are false
        //is optional and can be added
if(numA > 0){
    console.log("hello");
}else if (numH === 0){
    console.log("world");
}else{
    console.log("I will execute if all other condition are false");
}

//Esle statement should anly be added if there is a preceeding if coditiom\n.
//Else statements by itself will not work
// else {
//  console.log("Will this run")
// }

// else if (numH === 0){
//     console.log("hi iam else if")
// }else{
//     console.log("hi, im else")
// }

//there should be a preceeding if() first

//if , else if and else statement with function

let message = "No message.";
console.log(message);

function determineTyphoonIntensity(windspeed){

    if(windspeed < 30){
        return "not a typhoon yet!";
    } 
    else if(windspeed <= 61){
        return "tropical depression detected";
    } 
    else if(windspeed >= 62 && windspeed <= 88){
        return "tropical storm detected";
    }
    else if(windspeed >= 89 || windspeed <= 177){
        return "Sever tropical storm detected";
    }
    else {
        return "Typhon detected";
    }
}

message = determineTyphoonIntensity(110)
console.log(message)

if (message === "Sever tropical storm detected"){
    console.warn(message);
}

//console.warn is a good way to print a warning in our console that could help developer act on certain out within our code

// function checkOddOrEven(num){
//     if (num == num%2){
//         alert("Even Number" + num) 
//         console.log("Even Number" + num)
//     }else{
//         alert("Odd Number" + num) 
//        console.log("Odd Number" + num);
//   }
// }


// console.log(checkOddOrEven(3))

// function checkAge(age){
//     if (age <= 18) {
//         alert("Under Age")
//     return console.log(age <= 18)
//     }else{
//         alert(age > 18) 
//        return console.log(age > 18, age)
//     }
// }

// let isAllowedToDrink = checkAge(29);


//Thruthy and values

if(true){
    console.log("Truthy")
}if(1){
    console.log("! is Truthy")
}if([]){
    console.log("[]Truthy")
}

if("alpha"){
    console.log("Truthy")
}

if(false){
    console.log("Falsey")
}if(0){
    console.log("Falsey")
}if(undefined){
    console.log("Falsey")
}

//conditional Ternary operator 


let age =13;
let result = age < 18? "Underage": "legal age";
console.log(result)

//Switch Statement

// let day = prompt("Whta day of the week is it today?").toLocaleLowerCase();
// console.log(day);

// switch(day){
//     case'monday':
//         console.log("The color of the day red");
//         break;

//     case'tuesday':
//         console.log("The color of the day orange");
//         break;

//     case'wenesday':
//         console.log("The color of the day  yellow");
//         break;

//     case'thurstday':
//         console.log("The color of the day  pink");
//         break;

//     case'friday':
//         console.log("The color of the day  blue");
//         break;

//     case'saturday':
//         console.log("The color of the day  green");
//         break;

//     case'sunday':
//         console.log("The color of the day  yellow");
//         break;

// }



function showIntersityAlert(windspeed){
    try{
        allerat(determineTyphoonIntensity(windspeed))
    }catch(error){
        console.log(typeof error);
        console.warn(error.message);
    }finally{
        alert("Intensity update will show ne alert");
    }
}

showIntersityAlert(56)
console.log('hi')
